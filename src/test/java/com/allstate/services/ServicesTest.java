package com.allstate.services;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class ServicesTest {
    @Autowired
    private PaymentService services;
    //private com.allstate.services.CompactDiscService service;
    @Autowired
    private MongoTemplate mongoTemplate;



    @Test
    public void addPaymentThroughServiceLayer() {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss)");
        Date paymentDate = new Date();
        Payment payment = new Payment(1, paymentDate , "credit card", 100.20, 444);
        services.save(payment);
        Assert.assertNotNull("Check if payment is not found", payment);
    }

    @Test
    public void getById()
    {

        Payment payment = services.findById(1);
        assertEquals(1, payment.getId());
    }

    @Test
    public void getByType()
    {

        List<Payment> payments = services.findByType("credit card");
        assertEquals("credit card", payments.get(0).getType());
    }

    @Test
    public void getRowCount() {
        assertEquals(1, services.rowCount());
    }

    @After()
    public  void cleanUp() {
        for ( String collectionName : mongoTemplate.getCollectionNames()){
            if (!collectionName.startsWith("system.")){
                mongoTemplate.getCollection(collectionName).drop();
            }
        }
    }
}
