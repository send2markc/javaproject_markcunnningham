package com.allstate.rest;

import com.allstate.services.PaymentService;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@SpringBootTest
public class RestTest {

    //@LocalServerPort
    //private int port;

    @Autowired
    private PaymentService services;

    //@Autowired
    //private RestTemplate restTemplate;

   // @Value("${local.server.port}")
   // int port;

    @Autowired
    private PaymentController rest;


    private String createURLWithPort(String uri)
    {
        //return "http://localhost:" + port + uri;
        return "http://localhost:" + "8080" + uri;

    }

    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testStatus() throws JSONException {

        String status = rest.getStatus();

        Assert.assertEquals(true, status.contains("Payment Rest Api is running"));


//        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
//
//        ResponseEntity<String> response = restTemplate.exchange(
//                createURLWithPort("/api/status"), HttpMethod.GET, entity, String.class);
//
//        String expected = "Payment Rest Api is running";
//        JSONAssert.assertEquals(expected, response.getBody(), false);


//        final String baseUrl = "http://localhost:8080/api/status";
//        URI uri = new URI(baseUrl);
//
//        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
//
//        //String message = template.getForObject("http://localhost:" + port + "/", String.class);
//        Assert.assertEquals(true, result.getBody().contains("Payment Rest Api is running"));
    }


}
