package com.allstate.dao;

import com.allstate.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class DaoTest {
    @Autowired
    private PaymentDao dao;
    //private com.allstate.services.CompactDiscService service;
    @Autowired
    private  MongoTemplate mongoTemplate;


    @Test
    public void addPaymentThroughDaoLayer() {
        Date paymentDate = new Date();
        Payment payment = new Payment(2, paymentDate , "cheque", 100.20, 444);
        dao.save(payment);
        Assert.assertNotNull("Check if payment is not found", payment);
    }

    @Test
    public void getById()
    {

        Payment payment = dao.findById(2);
        assertEquals(2, payment.getId());
    }

    @Test
    public void getByType()
    {

        List<Payment> payments = dao.findByType("cheque");
        assertEquals("cheque", payments.get(0).getType());
    }

    @Test
    public void getRowCount() {
        //int rowCount = dao.rowCount();
        assertEquals(2, dao.rowCount());
    }

//    @After
//    public  void cleanUp() {
//        for ( String collectionName : mongoTemplate.getCollectionNames()){
//            if (!collectionName.startsWith("system.")){
//                mongoTemplate.getCollection(collectionName).drop();
//            }
//        }
//    }
}
