package com.allstate.dao;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDaoMongo implements PaymentDao {

   @Autowired
   private MongoTemplate mongoTemplate;


    @Override
    public int rowCount() {
        Query query = new Query();
        int result = (int)mongoTemplate.count(query, Payment.class);
        return result;
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = mongoTemplate.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> payments = mongoTemplate.find(query, Payment.class);
        return payments;
    }

    @Override
    public int save(Payment payment) {
        mongoTemplate.insert(payment);
        return 1;
    }
}
