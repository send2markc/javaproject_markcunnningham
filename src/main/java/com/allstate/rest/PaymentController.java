package com.allstate.rest;

import com.allstate.App;
import com.allstate.entities.Payment;
import com.allstate.services.PaymentService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(("/api"))
public class PaymentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    @Autowired
    private PaymentService paymentService ;


    @RequestMapping(value = "/rowCount", method = RequestMethod.GET)
    public int rowCount()
    {
        LOGGER.info("Get payment Rowcount ");

        return paymentService.rowCount();

    }

    @RequestMapping(value="/findbyid/{id}", method= RequestMethod.GET)
    public ResponseEntity<Payment> findByIdHandling404(@PathVariable("id") int id) {

        LOGGER.info("Payment find by id ");
        Payment payment = paymentService.findById(id);
        if (payment ==null) {
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }

    @RequestMapping(value="/findbytype/{type}", method= RequestMethod.GET)
    public ResponseEntity<List<Payment>> findByTypeHandling404(@PathVariable("type") String type) {
        LOGGER.info("Payment - find by type");
        List<Payment> payment = paymentService.findByType(type);
        if (payment ==null) {
            return new ResponseEntity<List<Payment>>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<List<Payment>>(payment, HttpStatus.OK);
        }
    }

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public void save(@RequestBody Payment payment) {
        paymentService.save(payment);
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        LOGGER.info("Payment - get status ");
        return "Payment Rest Api is running";
    }

}
