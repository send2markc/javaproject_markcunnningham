package com.allstate.services;

import com.allstate.entities.Payment;

import java.util.List;

public interface IPaymentService {


    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
