package com.allstate.services;

import com.allstate.dao.PaymentDao;
import com.allstate.dao.PaymentDaoMongo;
import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService implements IPaymentService{

    @Autowired
    private PaymentDao dao;


    public int rowCount(){ return dao.rowCount();};
    public Payment findById(int id)
    {
        if ( id > 0 ) {
            return dao.findById(id);
        }

        return null;
    }

    public List<Payment> findByType(String type)
    {
        if ( type != null ) {
            return dao.findByType(type);
        }

        return null;

    }
    public int save(Payment payment){ return dao.save(payment );};

}
